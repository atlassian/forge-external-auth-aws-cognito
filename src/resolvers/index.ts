import api from "@forge/api";
import Resolver from "@forge/resolver";

const resolver = new Resolver();

resolver.define("getData", async () => {
  const cognito = api.asUser().withProvider("cognito", "aws-cognito");
  if (!(await cognito.hasCredentials())) {
    await cognito.requestCredentials();
  }
  const response = await cognito.fetch(`/oauth2/userInfo`);
  if (response.ok) {
    return response.json();
  }
  return {
    status: response.status,
    statusText: response.statusText,
    text: await response.text(),
  };
});

export const handler = resolver.getDefinitions();
