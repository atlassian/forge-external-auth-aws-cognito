import React, { useEffect, useState } from "react";
import { invoke } from "@forge/bridge";
import ForgeReconciler, { CodeBlock } from "@forge/react";

const App = () => {
  const [data, setData] = useState(undefined);

  useEffect(() => {
    invoke("getData").then(setData);
  }, []);

  return (
    <CodeBlock
      text={JSON.stringify(data, null, 2)}
      language="json"
      showLineNumbers
    />
  );
};

ForgeReconciler.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

