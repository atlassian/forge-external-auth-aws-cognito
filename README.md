# Forge External Auth AWS Cognito Example

[![Atlassian license](https://img.shields.io/badge/license-Apache%202.0-blue.svg?style=flat-square)](LICENSE)

This is an example [Forge](https://developer.atlassian.com/platform/forge/) app that uses Amazon's AWS Cognito to authenticate a user.

See [developer.atlassian.com/platform/forge/external-auth/](https://developer.atlassian.com/platform/forge/external-auth/) for documentation and tutorials explaining using OAuth APIs with Forge.


## Requirements

See [Set up Forge](https://developer.atlassian.com/platform/forge/set-up-forge/) for instructions to get set up.

## Configure AWS Cognito

Create an AWS Cognito User Pool

- Add an "App client"
  - Ensure "Generate client secret" is checked
  - Ensure "Enable token revocation" is checked
  - Ensure "ALLOW_REFRESH_TOKEN_AUTH" is checked
- Once its created, go to "App client settings" under "App integration"
  - Set the callback URL to `https://id.atlassian.com/outboundAuth/finish, https://id.stg.internal.atlassian.com/outboundAuth/finish`
- Go to "Domain name" under "App integration"
  - Set a domain name to enable the web interface

## Setup the manifest

- Change the permissions and remote to point to the domain you set above
- Change the client ID to the one in the App Client from above

## Deploy the app

- Register a new App ID by running:
```
forge register
```

- Build and deploy your app by running:
```
forge deploy
```

- Install your app in an Atlassian site by running:
```
forge install
```

- Configure the client secret by running:
```
forge providers configure
```

- Develop your app by running `forge tunnel` to proxy invocations locally:
```
forge tunnel
```

### Notes
- Use the `forge deploy` command when you want to persist code changes.
- Use the `forge install` command when you want to install the app on a new site.
- Once the app is installed on a site, the site picks up the new app changes you deploy without needing to rerun the install command.

## Support

See [Get help](https://developer.atlassian.com/platform/forge/get-help/) for how to get help and provide feedback.

## License

Copyright (c) 2021 Atlassian and others.
Apache 2.0 licensed, see [LICENSE](LICENSE) file.

[![From Atlassian](https://raw.githubusercontent.com/atlassian-internal/oss-assets/master/banner-cheers.png)](https://www.atlassian.com)
